# Juego De La Vida 1.0
Este es el prototipo 1.0 del proyecto de DAM de la 1º Evaluacion para la asignatura de programacion.

Programa para consola que emula el juego de la vida.

Disponemos de 4 clases:
- Usuario : Clase para crear usuarios con ciertas caracteristicas.

- Simulacion: En esta clase se simulara el juego

- SesionInicio: Sirve para registar la sesion de el usuario que la inicia y la fecha en la que la inicia

- JVPrincipal:Clase principal, tambien se encuentran metodos de validacion de datos, busqueda de usuarios y un metodo para mostrar los usuarios registrados




 ## Caso de uso 

![alt_text](https://raw.githubusercontent.com/nachobelmonte/JuegoDeLaVidaProyectoDam1/master/JV2018/UML/JVida-CasoUso-Inicial.jpg)

## Modelo Conceptual

![alt_text](https://raw.githubusercontent.com/nachobelmonte/JuegoDeLaVidaProyectoDam1/master/JV2018/UML/modelo1.0-Conceptual.jpg)

## Diseño de clases 

![alt text](https://raw.githubusercontent.com/nachobelmonte/JuegoDeLaVidaProyectoDam1/master/JV2018/UML/modelo1.0-Dise%C3%B1oClases.jpg)
