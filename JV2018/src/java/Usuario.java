import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Proyecto: Juego de la vida. Implementa el concepto de Usuario según el
 * modelo1. En esta versión sólo se ha aplicado un diseño OO básico. Se pueden
 * detectar varios defectos y antipatrones de diseño: - Obsesión por los tipos
 * primitivos. - Clase demasiado grande. - Clase acaparadora, que delega poca
 * responsabilidad.
 * 
 * @since: prototipo1.0
 * @source: Usuario.java
 * @version: 1.0 - 2018/11/21
 * @author: nacho
 */

public class Usuario {

	private String nif;
	private String nombre;
	private String apellidos;
	private String domicilio;
	private String correo;
	private Calendar fechaNacimiento;
	private Calendar fechaAlta;
	private String claveAcceso;
	private String rol;

	final static String[] ROLES = { "INVITADO", "NORMAL", "ADMINISTRADOR" };

	public Usuario() {
		this.nif = "00000000T";
		this.nombre = "Nombre";
		this.apellidos = "Apellidos";
		this.domicilio = "Domicilio";
		this.correo = "correo@correo.es";
		this.fechaNacimiento = new GregorianCalendar();
		this.fechaAlta = new GregorianCalendar();
		this.claveAcceso = "Miau#0";
		this.rol = ROLES[1];
	}

	public Usuario(String nif, String nombre, String apellidos, String domicilio, String correo,
			Calendar fechaNacimiento, Calendar fechaAlta, String claveAcceso, String rol) {

		setNif(nif);
		setNombre(nombre);
		setApellidos(apellidos);
		setDomicilio(domicilio);
		setCorreo(correo);
		setFechaNacimiento(fechaNacimiento);
		setFechaAlta(fechaAlta);
		setClaveAcceso(claveAcceso);
		setRol(rol);
	}

	/**
	 * Setters
	 */

	public void setNif(String nif) {
		assert nif != null;
		if (nifValido(nif)) {
			this.nif = nif;
		}
		if (this.nif == null) {
			this.nif = "00000000T";
		}

	}

	public void setNombre(String nombre) {
		assert nombre != null;
		if (nombreValido(nombre)) {

			this.nombre = nombre;
		}
		if (this.nombre == null) {
			this.nombre = "Nombre";
		}
	}

	public void setApellidos(String apellidos) {
		assert apellidos != null;
		if (apellidosValidos(apellidos)) {

			this.apellidos = apellidos;
		}
		if (this.apellidos == null) {
			this.apellidos = "Apellidos";
		}
	}

	public void setDomicilio(String domicilio) {
		assert domicilio != null;
		if (domicilioValido(domicilio)) {
			this.domicilio = domicilio;
		}
		if (this.domicilio == null) {
			this.domicilio = "Domicilio";
		}
	}

	public void setCorreo(String correo) {
		assert correo != null;
		if (correoValido(correo)) {
			this.correo = correo;
		}
		if (this.correo == null) {
			this.correo = "correo@correo.es";
		}
	}

	public void setFechaNacimiento(Calendar fechaNacimiento) {
		assert fechaNacimiento != null;
		if (fechaNacimientoValida(fechaNacimiento)) {
			this.fechaNacimiento = fechaNacimiento;
		}
		if (this.fechaNacimiento == null) {
			this.fechaNacimiento = new GregorianCalendar();
		}
	}

	public void setFechaAlta(Calendar fechaAlta) {
		assert fechaAlta != null;
		if (fechaAltaValida(fechaAlta)) {
			this.fechaAlta = fechaAlta;
		}
		if (this.fechaAlta == null) {
			this.fechaAlta = new GregorianCalendar();
		}
	}

	public void setClaveAcceso(String claveAcceso) {
		assert claveAcceso != null;
		if (claveAccesoValida(claveAcceso)) {
			this.claveAcceso = claveAcceso;
		} else {
			this.claveAcceso = "Miau#0";
		}
	}

	public void setRol(String rol) {
		assert rol != null;
		if (rolValido(rol)) {
			this.rol = rol;
		} else {
			this.rol = ROLES[1];
		}
	}

	/**
	 * Getters
	 */

	public String getNif() {
		return nif;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public String getCorreo() {
		return correo;
	}

	public Calendar getFechaNacimiento() {
		return fechaNacimiento;
	}

	public Calendar getFechaAlta() {
		return fechaAlta;
	}

	public String getClaveAcceso() {
		return claveAcceso;
	}

	public String getRol() {
		return rol;
	}

	/**
	 * Metodos de validacion de datos
	 */

	private boolean nifValido(String nif) {
		return !nif.matches(".*[ ].*");
	}

	private boolean nombreValido(String nombre) {
		return !nombre.startsWith(" ");
	}

	private boolean apellidosValidos(String apellido) {
		return !apellido.startsWith(" ");
	}

	public boolean fechaAltaValida(Calendar fechaAlta) {
		if (fechaAlta.get(Calendar.YEAR) > 2018) {
			return false;
		}
		return true;
	}

	public boolean fechaNacimientoValida(Calendar fechaNacimiento) {
		if (fechaNacimiento.get(Calendar.YEAR) > 2018) {
			return false;
		}
		return true;
	}

	public boolean claveAccesoValida(String claveAcceso) {
		return !claveAcceso.matches(".*[ ].*");
	}

	public boolean correoValido(String correo) {
		return !correo.matches(".*[ ].*");
	}

	private boolean domicilioValido(String domicilio) {
		return !domicilio.startsWith(" ");
	}

	private boolean rolValido(String rol) {
		return rol.equals("INVITADO") || rol.equals("NORMAL") || rol.equals("ADMINISTRADOR");
	}

	/**
	 * Metodo toString
	 */
	@Override
	public String toString() {

		return "nif:             " + getNif() + "\n" + "nombre:          " + getNombre() + "\n" + "apellidos:       "
				+ getApellidos() + "\n" + "domicilio:       " + getDomicilio() + "\n" + "correo:          "
				+ getCorreo() + "\n" + "fechaNacimiento: " + getFechaNacimiento().get(Calendar.YEAR) + "."
				+ getFechaNacimiento().get(Calendar.MONTH) + "." + getFechaNacimiento().get(Calendar.DAY_OF_MONTH)
				+ "\n" + "fechaAlta:       " + getFechaAlta().get(Calendar.YEAR) + "."
				+ getFechaAlta().get(Calendar.MONTH) + "." + getFechaAlta().get(Calendar.DAY_OF_MONTH) + "\n"
				+ "claveAcceso:     " + getClaveAcceso() + "\n" + "rol:             " + getRol() + "\n";
	}

} // class