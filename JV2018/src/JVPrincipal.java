import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 * Proyecto: Juego de la vida. Implementa el control de inicio de sesión y
 * ejecución de la simulación por defecto. En esta versión sólo se ha aplicado
 * un diseño OO básico. Se pueden detectar varios defectos y antipatrones de
 * diseño: - Obsesión por los tipos primitivos. - Exceso de métodos estáticos. -
 * Clase acaparadora, que delega poca responsabilidad. - Clase demasiado grande.
 * 
 * @since: prototipo1.0
 * @source: JVPrincipal.java
 * @version: 1.0
 * @author: nacho
 */

public class JVPrincipal {

	static final int NUM_MAX_INTENTOS_INICIO_SESION = 3;
	static Usuario[] datosUsuarios = new Usuario[10];
	static SesionUsuario[] datosSesiones = new SesionUsuario[10];
	static int sesionesRegistradas = 0;
	static String nifUsuarioInicioSesion;
	static String claveUsuarioInicioSesion;
	static Scanner teclado = new Scanner(System.in);

	public static void main(String[] args) {

		cargarUsuariosPrueba();
		mostrarTodosDatosUsuarios();

		System.out.println("Nif usuario: ");
		nifUsuarioInicioSesion = teclado.nextLine();
		System.out.println("Contraseña: ");
		claveUsuarioInicioSesion = teclado.nextLine();

		if (inicioSesionCorrecto()) {

			registrarSesion();
			System.out.println("\n-Se ha iniciado sesion correctamente-\n");

			Simulacion simulacion = new Simulacion(buscarUsuario(nifUsuarioInicioSesion), new GregorianCalendar(),
					new byte[30][30],10);
			simulacion.arrancarDemo();
		}

	}

	/**
	 * Valida el inicio de sesion
	 * 
	 * @return
	 */
	private static boolean inicioSesionCorrecto() {

		if (buscarUsuario(nifUsuarioInicioSesion) != null) {

			if (buscarUsuario(nifUsuarioInicioSesion).getClaveAcceso().equals(claveUsuarioInicioSesion)) {

				return true;
			}
		}
		for (int i = 0; i < NUM_MAX_INTENTOS_INICIO_SESION; i++) {

			// Pide los datos de inicio de sesion
			System.out.println("\n-Datos erroneos, vuelve a introducirlos-\n");
			System.out.println("Nif usuario: ");
			nifUsuarioInicioSesion = teclado.nextLine();
			System.out.println("Contraseña: ");
			claveUsuarioInicioSesion = teclado.nextLine();

			if (buscarUsuario(nifUsuarioInicioSesion) != null) {

				if (buscarUsuario(nifUsuarioInicioSesion).getClaveAcceso().equals(claveUsuarioInicioSesion)) {

					return true;
				}
			}
		}
		System.out.println("\n-Has superado el limite de intentos-");
		return false;
	}

	/**
	 * Registra la sesion del usuario almacenandola en datosSesiones
	 */
	private static void registrarSesion() {

		datosSesiones[ultimaPosicionVaciaDatosSesiones()] = new SesionUsuario(buscarUsuario(nifUsuarioInicioSesion),
				new GregorianCalendar());
	}

	/**
	 * Dvuelve la posicion en el array del usuario introducido
	 * 
	 * @return error si no existe el usuario
	 */
	private static Usuario buscarUsuario(String nif) {

		for (int j = 0; j < sesionesRegistradas; j++) {

			if (nif.equals(datosUsuarios[j].getNif())) {

				return datosUsuarios[j];
			}
		}
		return null;
	}

	/**
	 * Busca la ultima pisicion vacia del array datosSesiones
	 * 
	 * @return
	 */
	public static int ultimaPosicionVaciaDatosSesiones() {

		for (int i = datosSesiones.length - 1; i != 0; i--) {
			if (datosSesiones == null) {
				return i;
			}
		}
		return 0;
	}

	/**
	 * Metodo para cargar los datos de los usuarios de prueba
	 */
	private static void cargarUsuariosPrueba() {

		String[] nifPrueba = { "00001a", "00002b", "00003c", "000004d", "00005e" };
		String[] nombrePrueba = { "usuario1", "usuario2", "usuario3", "usuario4", "usuario5" };
		String[] apellidosPrueba = { "apellido3", "apellido2", "apellido3", "apellido4", "apellido5" };
		String[] domicilioPrueba = { "calle1", "calle2", "calle3", "calle4", "calle5" };
		String[] correoPrueba = { "cooreo@1", "cooreo@2", "cooreo@3", "cooreo@4", "correo@5" };
		GregorianCalendar[] fechaNacimientoPrueba = { new GregorianCalendar(), new GregorianCalendar(),
				new GregorianCalendar(), new GregorianCalendar(), new GregorianCalendar(), };
		GregorianCalendar[] fechaAltaPrueba = { new GregorianCalendar(), new GregorianCalendar(),
				new GregorianCalendar(), new GregorianCalendar(), new GregorianCalendar(), };
		String[] claveAccesoPrueba = { "Miau#1", "Miau#2", "Miau#3", "Miau#4", "Miau#5" };
		String[] rolPrueba = { "NORMAL", "NORMAL", "NORMAL", "NORMAL", "NORMAL" };

		for (int i = 0; i < nombrePrueba.length; i++) {
			datosUsuarios[i] = new Usuario(nifPrueba[i], nombrePrueba[i], apellidosPrueba[i], domicilioPrueba[i],
					correoPrueba[i], fechaNacimientoPrueba[i], fechaAltaPrueba[i], claveAccesoPrueba[i], rolPrueba[i]);
			sesionesRegistradas++;
		}

	}

	/**
	 * Muesta los datos de los usuarios registrados
	 */
	private static void mostrarTodosDatosUsuarios() {

		for (int i = 0; i < sesionesRegistradas; i++) {
			System.out.println(datosUsuarios[i]);
		}
	}

} // class
