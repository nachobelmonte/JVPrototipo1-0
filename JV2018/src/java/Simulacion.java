import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Proyecto: Juego de la vida. Organiza aspectos de gestión de la simulación
 * según el modelo1. En esta versión sólo se ha aplicado un diseño OO básico. Se
 * pueden detectar varios defectos y antipatrones de diseño: - Obsesión por los
 * tipos primitivos. - Clase demasiado grande. - Clase acaparadora, que delega
 * poca responsabilidad.
 * 
 * @since: prototipo1.0
 * @source: Simulacion.java
 * @version: 1.0
 * @author: nacho
 */

public class Simulacion {

	private Usuario usr;
	private Calendar fecha;
	private byte[][] mundo;
	private int totalGeneraciones;

	// Reglas del juego
	final int REVIVIR = 3;
	final int MANTENER_VIVA = 2;

	/**
	 * Constructores
	 */

	public Simulacion() {

		setUsr(new Usuario());
		setFecha(new GregorianCalendar());
		setMundo(new byte[10][10]);
		setTotalGeneraciones(10);
	}

	public Simulacion(Usuario usr, Calendar fecha, byte[][] espacioMundo, int totalGeneraciones) {

		setUsr(usr);
		setFecha(fecha);
		setMundo(espacioMundo);
		setTotalGeneraciones(totalGeneraciones);
	}

	public Simulacion(Simulacion simulacion) {

		setUsr(simulacion.getUsr());
		setFecha(simulacion.getFecha());
		setMundo(simulacion.getMundo());
		setTotalGeneraciones(simulacion.getTotalGeneraciones());
	}

	/**
	 * Setters
	 */

	public void setUsr(Usuario usr) {
		assert usr != null;
		this.usr = usr;
	}

	public void setFecha(Calendar fecha) {
		assert fecha != null;
		this.fecha = fecha;
	}

	public void setMundo(byte[][] mundo) {
		assert mundo != null;

		if (mundoValido(mundo)) {

			this.mundo = mundo;
		}
		if (mundo == null) {

			this.mundo = new byte[10][10];
		}
	}

	public void setTotalGeneraciones(int totalGeneraciones) {
		assert totalGeneraciones > 0;
		this.totalGeneraciones = totalGeneraciones;
	}

	/**
	 * Getters
	 */

	public Usuario getUsr() {
		return usr;
	}

	public Calendar getFecha() {
		return fecha;
	}

	public byte[][] getMundo() {
		return mundo;
	}

	public int getTotalGeneraciones() {
		return totalGeneraciones;
	}

	/**
	 * Validacion de datos
	 */

	public boolean mundoValido(byte[][] mundo) {

		if (mundo.length > 3) {

			if (mundo[0].length > 3) {

				return true;
			}
		}
		return false;
	}

	/**
	 * Arranca la demo
	 */
	public void arrancarDemo() {

		cargarMundoDemo();

		for (int i = 0; i <= totalGeneraciones; i++) {

			if (i == 0 ) {
				System.out.println("Tiempo Incial");
			}else {
				System.out.println("Tiempo " + i);
			}
			mostrarMundo();
			actualizarMundo();

			try {
				Thread.sleep(60);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Muestra el mundo actual por pantalla
	 */
	private void mostrarMundo() {

		for (int i = 0; i < mundo.length; i++) {

			for (int j = 0; j < mundo[i].length; j++) {

				System.out.print(" ");

				if (mundo[i][j] == 1) {

					System.out.print("■");

				} else {

					System.out.print("□");
				}
			}
//			System.out.print("|");
			System.out.print("\n");
		}
	}

	/**
	 * Actualiza una unidad de tiempo el mundo
	 */
	private void actualizarMundo() {

		byte[][] mundoAux = new byte[mundo.length][mundo[0].length];

		for (int i = 0; i < mundo.length; i++) {
			for (int j = 0; j < mundo[i].length; j++) {

				int numVecinosVivos = numVecinosVivos(i, j);
				// Revivir celula
				if (numVecinosVivos == REVIVIR) {
					mundoAux[i][j] = 1;
				}
				// Mantener viva
				if (numVecinosVivos == MANTENER_VIVA) {
					mundoAux[i][j] = mundo[i][j];
				}
				// Matar celula
				if (numVecinosVivos != REVIVIR && numVecinosVivos != MANTENER_VIVA) {
					mundoAux[i][j] = 0;
				}
			}
		}
		mundo = mundoAux;
	}

	/**
	 * Carga las celulas vivas iniciales
	 */
	private void cargarMundoDemo() {

		mundo[1][2] = 1;
		mundo[3][1] = 1;
		mundo[3][2] = 1;
		mundo[3][5] = 1;
		mundo[3][6] = 1;
		mundo[3][7] = 1;
		mundo[2][4] = 1;
	}

	/**
	 * Calcula numero de vecinos vivos
	 * 
	 * @param coordenadasX
	 * @param coordenadasY
	 * @return numero de vecinos vivos
	 */
	private int numVecinosVivos(int coordenadasX, int coordenadasY) {

		int numVecinosVivos = 0;
		byte[] vecinos = caclularVecinos(coordenadasX, coordenadasY);
		for (int i = 0; i < vecinos.length; i++) {

			numVecinosVivos += vecinos[i];
		}
		return numVecinosVivos;
	}

	/**
	 * Cacula los vecinos de una celula
	 * 
	 * @param coordenadasX
	 * @param coordenadasY
	 * @return array con los valores de sus vecinos
	 */
	private byte[] caclularVecinos(int coordenadasX, int coordenadasY) {
		byte[] vecinos = new byte[8];

		/**
		 * Configuracion movimientos array para que sea un mundo infinito
		 */
		int derecha = coordenadasY != mundo[coordenadasX].length - 1 ? coordenadasY + 1 : 0;
		int izquierda = coordenadasY != 0 ? coordenadasY - 1 : mundo[coordenadasX].length - 1;
		int arriba = coordenadasX != 0 ? coordenadasX - 1 : mundo.length - 1;
		int abajo = coordenadasX != mundo.length - 1 ? coordenadasX + 1 : 0;

		vecinos[0] = mundo[coordenadasX][derecha];
		vecinos[1] = mundo[coordenadasX][izquierda];
		vecinos[2] = mundo[arriba][derecha];
		vecinos[3] = mundo[arriba][coordenadasY];
		vecinos[4] = mundo[arriba][izquierda];
		vecinos[5] = mundo[abajo][derecha];
		vecinos[6] = mundo[abajo][izquierda];
		vecinos[7] = mundo[abajo][coordenadasY];

		return vecinos;
	}

} // class
