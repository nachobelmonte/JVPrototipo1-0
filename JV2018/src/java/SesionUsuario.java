import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Proyecto: Juego de la vida. Implementa el concepto de SesionUsuario según el
 * modelo1 En esta versión sólo se ha aplicado un diseño OO básico. Se pueden
 * detectar varios defectos y antipatrones de diseño: - Clase perezosa, que
 * asume poca responsabilidad.
 * 
 * @since: prototipo1.0
 * @source: SesionUsuario.java
 * @version: 1.0
 * @author: nacho
 */

public class SesionUsuario {

	private Usuario usuario;
	private Calendar fecha;

	/**
	 * Constructores
	 */

	public SesionUsuario(Usuario usuario, Calendar fecha) {

		setFecha(fecha);
		setUsr(usuario);
	}

	public SesionUsuario() {

		setFecha(new GregorianCalendar());
		setUsr(new Usuario());
	}

	public SesionUsuario(SesionUsuario sesion) {

		setFecha(sesion.getFecha());
		setUsr(sesion.getUsr());
	}

	/**
	 * Setters
	 */
	
	public void setFecha(Calendar fecha) {
		assert fecha != null;
		this.fecha = fecha;
	}

	public void setUsr(Usuario usuario) {
		assert usuario != null;
		this.usuario = usuario;
	}

	/**
	 * Getters
	 */

	public Calendar getFecha() {
		return fecha;
	}

	public Usuario getUsr() {
		return usuario;
	}

} // class
